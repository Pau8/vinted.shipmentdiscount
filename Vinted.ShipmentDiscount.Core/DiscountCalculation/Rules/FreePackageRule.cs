﻿using Vinted.ShipmentDiscount.Core.Models;
using Vinted.ShipmentDiscount.Core.Services;

namespace Vinted.ShipmentDiscount.Core.DiscountCalculation.Rules
{
    public class FreePackageRule : IRule
    {
        private readonly int _every;
        private readonly PackageSize _packageSize;
        private readonly string _providerCode;
        private readonly ITransactionHistoryService _transactionHistoryService;

        public FreePackageRule(int every, PackageSize packageSize, string providerCode, ITransactionHistoryService transactionHistoryService)
        {
            _every = every;
            _packageSize = packageSize;
            _providerCode = providerCode;
            _transactionHistoryService = transactionHistoryService;
        }

        public decimal GetDiscount(Transaction transaction)
        {
            if (!transaction.IsValid || 
                transaction.PackageSize != _packageSize ||
                transaction.ShippingProvider.Code != _providerCode)
            {
                return 0;
            }

            var transactionNumber =
                _transactionHistoryService.GetAllTransactionsCountFor(transaction.ShippingProvider.Code,
                    transaction.PackageSize) + 1;

            var transactionNumberInThatMonth =
                _transactionHistoryService.GetTransactionsCountByMonthFor(transaction.ShippingProvider.Code,
                    transaction.PackageSize, transaction.Date) + 1;

            var isThirdTransaction = transactionNumber > 0 && transactionNumber % _every == 0;
            if (isThirdTransaction && transactionNumberInThatMonth <= _every)
            {
                return transaction.ShippingProvider.PriceByPackageSize[_packageSize];
            }

            return 0;
        }
    }
}