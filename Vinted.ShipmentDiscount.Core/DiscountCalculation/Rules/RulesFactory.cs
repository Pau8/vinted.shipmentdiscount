﻿using Vinted.ShipmentDiscount.Core.Constants;
using Vinted.ShipmentDiscount.Core.Models;
using Vinted.ShipmentDiscount.Core.Services;

namespace Vinted.ShipmentDiscount.Core.DiscountCalculation.Rules
{
    public class RulesFactory
    {
        private readonly IShippingProviderService _shippingProviderService;
        private readonly ITransactionHistoryService _transactionHistoryService;

        public RulesFactory(IShippingProviderService shippingProviderService, ITransactionHistoryService transactionHistoryService)
        {
            _shippingProviderService = shippingProviderService;
            _transactionHistoryService = transactionHistoryService;
        }

        public CheapestPriceRule CreateCheapestPriceForSmallPackage() => new CheapestPriceRule(PackageSize.S, _shippingProviderService);

        public FreePackageRule CreateFreeLargePackageForLPOnceIn3PerMonth() => new FreePackageRule(3, PackageSize.L, Carrier.LP, _transactionHistoryService);
    }
}