﻿using Vinted.ShipmentDiscount.Core.Models;
using Vinted.ShipmentDiscount.Core.Services;

namespace Vinted.ShipmentDiscount.Core.DiscountCalculation.Rules
{
    public class CheapestPriceRule : IRule
    {
        private readonly PackageSize _packageSize;
        private readonly decimal _cheapestPrice;

        public CheapestPriceRule(PackageSize packageSize, IShippingProviderService shippingProviderService) 
        {
            _packageSize = packageSize;
            _cheapestPrice = shippingProviderService.FindCheapestProviderPriceFor(packageSize);
        }

        public decimal GetDiscount(Transaction transaction)
        {
            if (!transaction.IsValid || transaction.PackageSize != _packageSize)
            {
                return 0;
            }

            return transaction.ShippingProvider.PriceByPackageSize[_packageSize] - _cheapestPrice;
        }
    }
}