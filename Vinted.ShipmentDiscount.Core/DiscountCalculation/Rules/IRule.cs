﻿using Vinted.ShipmentDiscount.Core.Models;

namespace Vinted.ShipmentDiscount.Core.DiscountCalculation.Rules
{
    public interface IRule
    {
        decimal GetDiscount(Transaction transaction);
    }
}
