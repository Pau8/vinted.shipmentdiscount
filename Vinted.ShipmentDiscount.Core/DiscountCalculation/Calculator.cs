﻿using System;
using System.Collections.Generic;
using Vinted.ShipmentDiscount.Core.DiscountCalculation.Rules;
using Vinted.ShipmentDiscount.Core.Models;

namespace Vinted.ShipmentDiscount.Core.DiscountCalculation
{
    public class Calculator
    {
        private readonly List<IRule> _rules = new List<IRule>();

        public void AddRule(IRule rule)
        {
            if (_rules.Contains(rule))
            {
                return;
            }

            _rules.Add(rule);
        }

        public decimal ApplyRules(Transaction transaction)
        {
            decimal discount = 0;

            foreach (var rule in _rules)
            {
                discount = Math.Max(rule.GetDiscount(transaction), discount);
            }

            return discount;
        }
    }
}
