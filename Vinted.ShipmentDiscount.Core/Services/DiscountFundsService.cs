﻿using System;

namespace Vinted.ShipmentDiscount.Core.Services
{
    public class DiscountFundsService : IDiscountFundsService
    {
        private const decimal MonthlyFundsLimit = 10;

        private decimal _remainingFunds;

        private DateTime _lastTransactionDate;

        public DiscountFundsService()
        {
            _remainingFunds = MonthlyFundsLimit;
        }

        public decimal GetPossibleDiscount(DateTime transactionDate, decimal discount)
        {
            ResetFundsIfNeeded(transactionDate);

            if (discount > _remainingFunds)
            {
                discount = _remainingFunds;
            }

            _remainingFunds -= discount;
            _lastTransactionDate = transactionDate;

            return discount;
        }

        private void ResetFundsIfNeeded(DateTime transactionDate)
        {
            var isFirstTransactionOfTheMonth = _lastTransactionDate.Year != transactionDate.Year ||
                                               _lastTransactionDate.Month != transactionDate.Month;

            if (isFirstTransactionOfTheMonth)
            {
                _remainingFunds = MonthlyFundsLimit;
            }
        }
    }
}
