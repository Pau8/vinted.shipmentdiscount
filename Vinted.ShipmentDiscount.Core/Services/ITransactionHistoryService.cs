﻿using System;
using System.Collections.Generic;
using Vinted.ShipmentDiscount.Core.Models;

namespace Vinted.ShipmentDiscount.Core.Services
{
    public interface ITransactionHistoryService
    {
        void Add(TransactionWithAppliedDiscount transaction);

        IEnumerable<TransactionWithAppliedDiscount> GetAll();

        int GetAllTransactionsCountFor(string shippingProviderCode, PackageSize packageSize);

        int GetTransactionsCountByMonthFor(string shippingProviderCode, PackageSize packageSize, DateTime date);
    }
}