﻿using System;

namespace Vinted.ShipmentDiscount.Core.Services
{
    public interface IDiscountFundsService
    {
        decimal GetPossibleDiscount(DateTime transactionDate, decimal discount);
    }
}