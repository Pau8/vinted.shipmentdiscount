﻿using Vinted.ShipmentDiscount.Core.Models;

namespace Vinted.ShipmentDiscount.Core.Services
{
    public interface IShippingProviderService
    {
        decimal FindCheapestProviderPriceFor(PackageSize packageSize);
    }
}
