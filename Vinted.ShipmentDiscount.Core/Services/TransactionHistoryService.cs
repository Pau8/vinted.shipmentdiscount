﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vinted.ShipmentDiscount.Core.Models;

namespace Vinted.ShipmentDiscount.Core.Services
{
    public class TransactionHistoryService : ITransactionHistoryService
    {
        private readonly List<TransactionWithAppliedDiscount> _transactions = new List<TransactionWithAppliedDiscount>();

        public void Add(TransactionWithAppliedDiscount transaction) => _transactions.Add(transaction);

        public IEnumerable<TransactionWithAppliedDiscount> GetAll() => _transactions;

        public int GetAllTransactionsCountFor(string shippingProviderCode, PackageSize packageSize) =>
            _transactions.Count(x => x.ShippingProvider.Code == shippingProviderCode && x.PackageSize == packageSize);

        public int GetTransactionsCountByMonthFor(string shippingProviderCode, PackageSize packageSize, DateTime date)
        {
            return _transactions.Count(x => x.ShippingProvider.Code == shippingProviderCode 
                                            && x.PackageSize == packageSize
                                            && x.Date.Year == date.Year 
                                            && x.Date.Month == date.Month);
        }
    }
}
