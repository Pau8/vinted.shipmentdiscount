﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vinted.ShipmentDiscount.Core.Models;

namespace Vinted.ShipmentDiscount.Core.Services
{
    public class ShippingProviderService : IShippingProviderService
    {
        private readonly IReadOnlyCollection<ShippingProvider> _shippingProviders;

        public ShippingProviderService(IReadOnlyCollection<ShippingProvider> shippingProviders)
        {
            _shippingProviders = shippingProviders;
        }

        public decimal FindCheapestProviderPriceFor(PackageSize packageSize)
        {
            var cheapest = _shippingProviders.FirstOrDefault(x => x.PriceByPackageSize.ContainsKey(packageSize));
            if (cheapest == null)
            {
                throw new InvalidOperationException("No providers for small package found.");
            }

            foreach (var provider in _shippingProviders.Where(x => x.PriceByPackageSize.ContainsKey(packageSize) && x.Code != cheapest.Code))
            {
                if (provider.PriceByPackageSize[packageSize] < cheapest.PriceByPackageSize[packageSize])
                {
                    cheapest = provider;
                }
            }

            return cheapest.PriceByPackageSize[packageSize];
        }
    }
}