﻿using System;

namespace Vinted.ShipmentDiscount.Core.Models
{
    public class TransactionWithAppliedDiscount : Transaction
    {
        public decimal DiscountedPrice { get; }

        public decimal Discount { get; }

        public TransactionWithAppliedDiscount(DateTime date, PackageSize packageSize, ShippingProvider shippingProvider,
            decimal discountedPrice, decimal discount)
            : base(date, packageSize, shippingProvider)
        {
            DiscountedPrice = discountedPrice;
            Discount = discount;
        }

        /// <summary>
        /// Original Input that could not be successfully parsed.
        /// </summary>
        public TransactionWithAppliedDiscount(string originalInput)
            : base(originalInput)
        {
        }
    }
}
