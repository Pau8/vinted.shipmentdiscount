﻿namespace Vinted.ShipmentDiscount.Core.Models
{
    public enum PackageSize
    {
        Unknown = 0,
        S = 1,
        M = 2,
        L = 3
    }
}