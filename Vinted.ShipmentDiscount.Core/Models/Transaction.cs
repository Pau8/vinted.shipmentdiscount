﻿using System;

namespace Vinted.ShipmentDiscount.Core.Models
{
    public class Transaction
    {
        public Transaction(DateTime date, PackageSize packageSize, ShippingProvider shippingProvider)
        {
            Date = date;
            PackageSize = packageSize;
            ShippingProvider = shippingProvider;
            IsValid = true;
        }

        /// <summary>
        /// Original Input that could not be successfully parsed.
        /// </summary>
        public Transaction(string originalInput)
        {
            OriginalInput = originalInput;
            IsValid = false;
        }
        
        public DateTime Date { get; }

        public PackageSize PackageSize { get; }

        public ShippingProvider ShippingProvider { get; }

        public bool IsValid { get; }

        public string OriginalInput { get; }
    }
}
