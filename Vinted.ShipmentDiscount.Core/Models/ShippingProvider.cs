﻿using System;
using System.Collections.Generic;

namespace Vinted.ShipmentDiscount.Core.Models
{
    public class ShippingProvider
    {
        public string Code { get; }

        public Dictionary<PackageSize, decimal> PriceByPackageSize { get; }

        public ShippingProvider(string code)
        {
            Code = code ?? throw new ArgumentNullException();
            PriceByPackageSize = new Dictionary<PackageSize, decimal>();
        }

        public void AddPrice(PackageSize packageSize, decimal price)
        {
            PriceByPackageSize.Add(packageSize, price);
        }
    }
}
