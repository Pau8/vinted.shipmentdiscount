﻿using System.Collections.Generic;
using Vinted.ShipmentDiscount.Core.DiscountCalculation;
using Vinted.ShipmentDiscount.Core.DiscountCalculation.Rules;
using Vinted.ShipmentDiscount.Core.Models;
using Vinted.ShipmentDiscount.Core.Services;
using Vinted.ShipmentDiscount.Infrastructure;

namespace Vinted.ShipmentDiscount.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var dataProvider = new FileProvider();
            var providers = dataProvider.GetShippingProviders();
            var transactions = dataProvider.GetTransactions(providers);
            var transactionHistoryService = new TransactionHistoryService();
            var discountFundsService = new DiscountFundsService();
            var shippingProviderService = new ShippingProviderService(providers);
            var rulesFactory = new RulesFactory(shippingProviderService, transactionHistoryService);
            var calculator = new Calculator();
            calculator.AddRule(rulesFactory.CreateCheapestPriceForSmallPackage());
            calculator.AddRule(rulesFactory.CreateFreeLargePackageForLPOnceIn3PerMonth());

            foreach (var transaction in transactions)
            {
                if (!transaction.IsValid)
                {
                    transactionHistoryService.Add(new TransactionWithAppliedDiscount(transaction.OriginalInput));
                    continue;
                }

                var potentialDiscount = calculator.ApplyRules(transaction);
                var discount = discountFundsService.GetPossibleDiscount(transaction.Date, potentialDiscount);
                var discountedPrice = transaction.ShippingProvider.PriceByPackageSize[transaction.PackageSize] - discount;

                transactionHistoryService.Add(new TransactionWithAppliedDiscount(
                    transaction.Date, transaction.PackageSize, transaction.ShippingProvider,
                    discountedPrice, discount));
            }

            PrintResults(transactionHistoryService.GetAll());
            System.Console.ReadKey();
        }

        private static void PrintResults(IEnumerable<TransactionWithAppliedDiscount> transactionsWithAppliedDiscount)
        {
            foreach (var item in transactionsWithAppliedDiscount)
            {
                if (!item.IsValid)
                {
                    System.Console.WriteLine($"{item.OriginalInput} Ignored");
                    continue;
                }

                var discount = item.Discount == 0 
                    ? "-" 
                    : item.Discount.ToString("F");

                System.Console.WriteLine(
                    $"{item.Date:yyyy-MM-dd} {item.PackageSize.ToString()} {item.ShippingProvider.Code} {item.DiscountedPrice:F} {discount}");
            }
        }
    }
}
