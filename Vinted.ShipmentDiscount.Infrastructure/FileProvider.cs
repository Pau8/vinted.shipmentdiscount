﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Vinted.ShipmentDiscount.Core.Models;

namespace Vinted.ShipmentDiscount.Infrastructure
{
    public class FileProvider : IDataProvider
    {
        private readonly string _transactionsFilePath;
        private readonly string _providersFilePath;

        public FileProvider()
        {
            var assemblyDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ??
                                    throw new ArgumentNullException();

            const string dataFolder = "Data";
            _transactionsFilePath = Path.Combine(assemblyDirectory, dataFolder, "input.txt");
            _providersFilePath = Path.Combine(assemblyDirectory, dataFolder, "providers.txt");
        }

        public List<Transaction> GetTransactions(List<ShippingProvider> shippingProviders)
        {
            using (var reader = new StreamReader(_transactionsFilePath))
            {
                var transactions = new List<Transaction>();
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (string.IsNullOrWhiteSpace(line))
                    {
                        continue;
                    }

                    var transactionParts = line.Split(null)
                        .Where(x => !string.IsNullOrWhiteSpace(x))
                        .ToList();

                    var transaction = !TryParseTransaction(transactionParts, shippingProviders, out var date,
                        out var packageSize, out var shippingProvider)
                        ? new Transaction(line)
                        : new Transaction(date, packageSize, shippingProvider);

                    transactions.Add(transaction);
                }

                return transactions;
            }
        }

        public List<ShippingProvider> GetShippingProviders()
        {
            using (var reader = new StreamReader(_providersFilePath))
            {
                var shippingProviders = new List<ShippingProvider>();
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    if (string.IsNullOrWhiteSpace(line))
                    {
                        continue;
                    }

                    var shippingProviderLines = line.Split(null)
                        .Where(x => !string.IsNullOrWhiteSpace(x))
                        .ToList();

                    if (shippingProviderLines.Count != 3)
                    {
                        throw new FormatException("Unexpected columns count in shipping providers data file.");
                    }

                    var shippingProviderCode = shippingProviderLines[0];

                    var existingShippingProvider = shippingProviders.FirstOrDefault(x => x.Code == shippingProviderCode);
                    if (existingShippingProvider == default)
                    {
                        existingShippingProvider = new ShippingProvider(shippingProviderCode);
                        shippingProviders.Add(existingShippingProvider);
                    }
                   
                    if (!Enum.TryParse(shippingProviderLines[1], out PackageSize packageSize))
                    {
                        throw new FormatException(nameof(packageSize));
                    }

                    if (!decimal.TryParse(shippingProviderLines[2], out var price))
                    {
                        throw new FormatException(nameof(price));
                    }

                    existingShippingProvider.AddPrice(packageSize, price);
                }

                return shippingProviders;
            }
        }

        private static bool TryParseTransaction(IReadOnlyList<string> transactionParts,
            IEnumerable<ShippingProvider> shippingProviders, out DateTime date, out PackageSize packageSize,
            out ShippingProvider shippingProvider)
        {
            date = default;
            packageSize = default;
            shippingProvider = default;

            if (transactionParts.Count != 3)
            {
                return false;
            }

            if (!DateTime.TryParse(transactionParts[0], out date))
            {
                return false;
            }

            if (!Enum.TryParse(transactionParts[1], out packageSize))
            {
                return false;
            }

            shippingProvider = shippingProviders.FirstOrDefault(x => x.Code == transactionParts[2]);
            if (shippingProvider == default || !shippingProvider.PriceByPackageSize.ContainsKey(packageSize))
            {
                return false;
            }

            return true;
        }
    }
}
