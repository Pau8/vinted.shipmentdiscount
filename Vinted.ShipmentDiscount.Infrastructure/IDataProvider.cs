﻿using System.Collections.Generic;
using Vinted.ShipmentDiscount.Core.Models;

namespace Vinted.ShipmentDiscount.Infrastructure
{
    public interface IDataProvider
    {
        List<Transaction> GetTransactions(List<ShippingProvider> shippingProviders);

        List<ShippingProvider> GetShippingProviders();
    }
}
