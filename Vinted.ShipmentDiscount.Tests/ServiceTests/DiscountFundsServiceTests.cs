﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Vinted.ShipmentDiscount.Core.Services;

namespace Vinted.ShipmentDiscount.Core.Tests.ServiceTests
{
    [TestFixture]
    public class DiscountFundsServiceTests
    {
        private DiscountFundsService _service;

        [SetUp]
        public void SetUp()
        {
            _service = new DiscountFundsService();
        }

        [Test]
        public void GetPossibleDiscount_WantedDiscountExceedsLimit_ReturnsPossibleCoverage()
        {
            var discount = _service.GetPossibleDiscount(DateTime.Now, 13);
            Assert.AreEqual(10, discount);
        }

        [Test]
        public void GetPossibleDiscount_WantedDiscountBelowLimit_ReturnsWantedDiscount()
        {
            var discount = _service.GetPossibleDiscount(DateTime.Now, 8);
            Assert.AreEqual(8, discount);
        }

        [Test]
        public void GetPossibleDiscount_MultipleDiscounts_FundRemainingResetsCorrectly()
        {
            var discounts = new Dictionary<DateTime, decimal>
            {
                {DateTime.Parse("2000-01-01"), 3}, // => 3
                {DateTime.Parse("2000-01-02"), 3}, // => 3
                {DateTime.Parse("2000-01-03"), 3}, // => 3
                {DateTime.Parse("2000-01-04"), 3}, // => 1
                {DateTime.Parse("2000-02-03"), 3}  // => 3
            };

            decimal totalDiscount = 0;
            foreach (var (date, value) in discounts)
            {
                totalDiscount += _service.GetPossibleDiscount(date, value);
            }

            Assert.AreEqual(13, totalDiscount);
        }
    }
}
