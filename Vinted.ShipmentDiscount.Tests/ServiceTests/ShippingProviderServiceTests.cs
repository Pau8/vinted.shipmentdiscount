﻿using System.Collections.Generic;
using NUnit.Framework;
using Vinted.ShipmentDiscount.Core.Models;
using Vinted.ShipmentDiscount.Core.Services;

namespace Vinted.ShipmentDiscount.Core.Tests.ServiceTests
{
    [TestFixture]
    public class ShippingProviderServiceTests
    {
        [Test]
        public void FindCheapestProviderPriceFor_SmallPackage_ReturnsCheapest()
        {
            var lp = new ShippingProvider("LP");
            lp.AddPrice(PackageSize.S, 4);
            var mr = new ShippingProvider("MR");
            mr.AddPrice(PackageSize.S, 3);
            var dpd = new ShippingProvider("DPD");
            dpd.AddPrice(PackageSize.S, 8);
            var providers = new List<ShippingProvider> {lp, mr, dpd};

            var shippingProviderService = new ShippingProviderService(providers);

            var result = shippingProviderService.FindCheapestProviderPriceFor(PackageSize.S);

            Assert.AreEqual(3, result);
        }
    }
}
