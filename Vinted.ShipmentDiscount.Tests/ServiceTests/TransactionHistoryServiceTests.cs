﻿using System;
using NUnit.Framework;
using Vinted.ShipmentDiscount.Core.Models;
using Vinted.ShipmentDiscount.Core.Services;

namespace Vinted.ShipmentDiscount.Core.Tests.ServiceTests
{
    [TestFixture]
    public class TransactionHistoryServiceTests
    {
        private TransactionHistoryService _service;

        [SetUp]
        public void SetUp()
        {
            _service = new TransactionHistoryService();
        }

        [Test]
        public void GetAllTransactionsCountFor_LargePackageViaLP_ReturnsCorrectCount()
        {
            const string providerCode = "LP";
            const PackageSize largePackage = PackageSize.L;
            var date = DateTime.Parse("2000-01-01");

            var transaction1 = new TransactionWithAppliedDiscount(date, largePackage,
                new ShippingProvider(providerCode), 0, 0);
            var transaction2 = new TransactionWithAppliedDiscount(date, largePackage,
                new ShippingProvider("Other"), 0, 0);
            var transaction3 = new TransactionWithAppliedDiscount(date, largePackage,
                new ShippingProvider(providerCode), 0, 0);
            var transaction4 = new TransactionWithAppliedDiscount(date, PackageSize.S,
                new ShippingProvider(providerCode), 0, 0);

            _service.Add(transaction1);
            _service.Add(transaction2);
            _service.Add(transaction3);
            _service.Add(transaction4);

            var count = _service.GetAllTransactionsCountFor(providerCode, largePackage);

            Assert.AreEqual(2, count);
        }
            
        [Test]
        public void GetTransactionsCountByMonthFor_LargePackageViaLP_ReturnsCorrectCount()
        {
            const string providerCode = "LP";
            const PackageSize largePackage = PackageSize.L;
            
            var transaction1 = new TransactionWithAppliedDiscount(DateTime.Parse("2000-01-01"), largePackage,
                new ShippingProvider(providerCode), 0, 0);
            var transaction2 = new TransactionWithAppliedDiscount(DateTime.Parse("2000-03-01"), largePackage,
                new ShippingProvider("Other"), 0, 0);
            var transaction3 = new TransactionWithAppliedDiscount(DateTime.Parse("2000-03-02"), largePackage,
                new ShippingProvider(providerCode), 0, 0);
            var transaction4 = new TransactionWithAppliedDiscount(DateTime.Parse("2000-04-01"), PackageSize.S,
                new ShippingProvider(providerCode), 0, 0);

            _service.Add(transaction1);
            _service.Add(transaction2);
            _service.Add(transaction3);
            _service.Add(transaction4);

            var count = _service.GetTransactionsCountByMonthFor(providerCode, largePackage,
                DateTime.Parse("2000-03-01"));

            Assert.AreEqual(1, count);
        }
    }
}
