﻿using System;
using Moq;
using NUnit.Framework;
using Vinted.ShipmentDiscount.Core.DiscountCalculation.Rules;
using Vinted.ShipmentDiscount.Core.Models;
using Vinted.ShipmentDiscount.Core.Services;

namespace Vinted.ShipmentDiscount.Core.Tests.RuleTests
{
    [TestFixture]
    public class CheapestPriceRuleTests
    {
        [Test]
        public void GetDiscount_ForSmallPackage_ReturnsDiscountToMatchCheapestProviderPrice()
        {
            const PackageSize smallPackage = PackageSize.S;

            var shippingProviderServiceMock = new Mock<IShippingProviderService>();
            shippingProviderServiceMock
                .Setup(x => x.FindCheapestProviderPriceFor(It.IsAny<PackageSize>()))
                .Returns(3);

            var shippingProvider = new ShippingProvider("LP");
            shippingProvider.AddPrice(smallPackage, 10);

            var cheapestPriceRule = new CheapestPriceRule(smallPackage, shippingProviderServiceMock.Object);
            var transaction = new Transaction(DateTime.Now, PackageSize.S, shippingProvider);
            var discount = cheapestPriceRule.GetDiscount(transaction);

            Assert.AreEqual(7, discount);
        }
    }
}
