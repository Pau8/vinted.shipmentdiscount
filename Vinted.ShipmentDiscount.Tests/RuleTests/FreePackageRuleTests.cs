﻿using System;
using Moq;
using NUnit.Framework;
using Vinted.ShipmentDiscount.Core.DiscountCalculation.Rules;
using Vinted.ShipmentDiscount.Core.Models;
using Vinted.ShipmentDiscount.Core.Services;

namespace Vinted.ShipmentDiscount.Core.Tests.RuleTests
{
    [TestFixture]
    public class FreePackageRuleTests
    {
        private Transaction _transaction;
        private FreePackageRule _freePackageRule;
        private Mock<ITransactionHistoryService> _transactionHistoryMock;
        private const decimal ShippingPrice = 10;

        [SetUp]
        public void SetUp()
        {
            const PackageSize largePackage = PackageSize.L;
            const string providerCode = "LP";

            var shippingProvider = new ShippingProvider(providerCode);
            shippingProvider.AddPrice(largePackage, ShippingPrice);

            _transactionHistoryMock = new Mock<ITransactionHistoryService>();
            _freePackageRule = new FreePackageRule(3, largePackage, providerCode, _transactionHistoryMock.Object);
            _transaction = new Transaction(DateTime.Now, largePackage, shippingProvider);
        }

        [Test]
        public void GetDiscount_OnlyOneTransaction_DiscountIsNotApplied()
        {
            var discount = _freePackageRule.GetDiscount(_transaction);
            Assert.AreEqual(0, discount);
        }

        [Test]
        public void GetDiscount_ThirdTransactionInAMonth_ShippingIsFree()
        {
            _transactionHistoryMock
                .Setup(x => x.GetAllTransactionsCountFor(It.IsAny<string>(), It.IsAny<PackageSize>()))
                .Returns(2);
          
            var discount = _freePackageRule.GetDiscount(_transaction);
            
            Assert.AreEqual(ShippingPrice, discount);
        }

        [Test]
        public void GetDiscount_NotThirdTransaction_NoDiscount()
        {
            _transactionHistoryMock
                .Setup(x => x.GetAllTransactionsCountFor(It.IsAny<string>(), It.IsAny<PackageSize>()))
                .Returns(7);

            var discount = _freePackageRule.GetDiscount(_transaction);
            
            Assert.AreEqual(0, discount);
        }

        [Test]
        public void GetDiscount_ThirdTransactionButTwicePerMonth_NoDiscount()
        {
            _transactionHistoryMock
                .Setup(x => x.GetAllTransactionsCountFor(It.IsAny<string>(), It.IsAny<PackageSize>()))
                .Returns(5);

            _transactionHistoryMock
                .Setup(x => x.GetTransactionsCountByMonthFor(
                    It.IsAny<string>(),
                    It.IsAny<PackageSize>(),
                    It.IsAny<DateTime>()))
                .Returns(4);

            var discount = _freePackageRule.GetDiscount(_transaction);

            Assert.AreEqual(0, discount);
        }
    }
}
